## Purpose
Kata Konf is an extensible, type-safe api for parsing configuration properties from a variety of sources: 
- program arguments
- environment variables
- system properties
- yaml/json files (via extension library)
- secrets from vault (via extension library)

Let's look at a few examples to see what the api provides
## Examples
Parsing multiple configuration values into a class
```kotlin
class ClientKonf(konf: KataKonf) {
  val port: Int by konf.env("server.port")
  val uri: String by konf.sysprop("server.uri")
  val connectionTimeout: Long by konf.sysprop("server.connectionTimeout")
}
```

Parsing configuration as an expression
```kotlin
val konf = KataKonfSources {
  EnvVarLens.source(EnvVarSource())
}
val port: Int = konf.env("server.port")
```

## Program Arguments
JVM programs usually contain a _main_ method accepting an `Array<String>` argument.  A `KataKonf` instance 
configured with a `ProgramArgsLens` will allow for the program arguments to be parsed.

### Setup
```kotlin
val args: Array<String> = ...
KataKonfDsl {
  ProgramArgsLens.source(args)
}
```

### Usage
```kotlin
class MyExampleKonf(konf: KataKonf) {
  val a: Int by konf.arg(0)
  val b: String by konf.arg(1) { s: String -> uppercase() }
  val c: Char? by konf.nullable.arg(100)
}
```
Notice that if the field type is nullable, `Char?`, then prefix `arg(100)` with `nullable`.  This is done to help
Kotlin's type inference!

## Environment Variables
JVM applications can access system environment variables and a `KataKonf` instance can be configured to allow these
environment variables to be parsed

### Setup
```kotlin
KataKonfDsl {
  EnvVarLens.source(EnvVarSource())
}
```

### Usage
```kotlin
class MyExampleKonf(konf: KataKonf) {
  val a: Int by konf.env("env-var-name-here")
  val b: String by konf.env("foo") { s: String -> uppercase() }
  val c: Char? by konf.nullable.env("not-found")
}
```
Notice that if the field type is nullable, `Char?`, then prefix `env("not-found")` with `nullable`.  This is done to help
Kotlin's type inference!

## System Properties
JVM applications can access system environment variables and a `KataKonf` instance can be configured to allow these
environment variables to be parsed

### Setup
```kotlin
KataKonfDsl {
  SystemPropertyLens.source(SystemPropertiesSource())
}
```

### Usage
```kotlin
class MyExampleKonf(konf: KataKonf) {
  val a: Int by konf.sysprop("env-var-name-here")
  val b: String by konf.sysprop("foo") { s: String -> uppercase() }
  val c: Char? by konf.nullable.sysprop("not-found")
}
```
Notice that if the field type is nullable, `Char?`, then prefix `sysprop("not-found")` with `nullable`.  This is done to help
Kotlin's type inference!