package tech.codingzen.kata.konfig

import tech.codingzen.kata.result.ResDsl
import kotlin.properties.PropertyDelegateProvider
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

typealias KataKonfRootId = String
typealias KataKonfRoot = Any
typealias KataKonfPropertyDelegateProvider<V> = PropertyDelegateProvider<Any?, ReadOnlyProperty<Any?, V>>
typealias ContextualParser<S, V> = ResDsl.(Any, KProperty<*>, S?) -> V
typealias ValueParser<S, V> = ResDsl.(S) -> V
