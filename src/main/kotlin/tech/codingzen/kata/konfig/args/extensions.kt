package tech.codingzen.kata.konfig.args

import tech.codingzen.kata.konfig.*
import tech.codingzen.kata.konfig.StringParsers.parser

inline fun <reified V : Any?> KataKonf.argParser(
  index: Int,
  crossinline parser: ContextualParser<String, V>
) = propertyDelegateParser { thisRef, property ->
  val args = ProgramArgsLens[this@argParser].value()
  val src = if (index in args.indices) args[index] else null
  parser(thisRef, property, src)
}

inline fun <reified V : Any?> KataKonf.arg(
  index: Int,
  crossinline parser: ValueParser<String, V>
) = argParser(index) { _, property, src ->
  src?.let { parser(it) } ?: run {
    if (property.returnType.isMarkedNullable) null as V
    else err("arg: src for index: $index is null yet the return type is not marked as nullable!!")
  }
}

inline fun <reified V : Any?> KataKonf.arg(index: Int) = arg<V>(index, parser())