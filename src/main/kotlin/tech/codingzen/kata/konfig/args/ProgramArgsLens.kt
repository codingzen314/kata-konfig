package tech.codingzen.kata.konfig.args

import tech.codingzen.kata.konfig.KataKonfLens

object ProgramArgsLens : KataKonfLens.Delegate<Array<String>> {
  override val lens = KataKonfLens<Array<String>>("ProgramArgs")
}