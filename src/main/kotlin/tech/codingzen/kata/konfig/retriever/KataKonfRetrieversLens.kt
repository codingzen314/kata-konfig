package tech.codingzen.kata.konfig.retriever

import tech.codingzen.kata.konfig.KataKonfLens

object KataKonfRetrieversLens: KataKonfLens.Delegate<KataKonfRetriever> {
  override val lens: KataKonfLens<KataKonfRetriever> = KataKonfLens("retrievers")
}