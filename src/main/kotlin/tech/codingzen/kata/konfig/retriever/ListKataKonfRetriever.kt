package tech.codingzen.kata.konfig.retriever

class ListKataKonfRetriever(val list: List<KataKonfRetriever>): KataKonfRetriever {
  override fun retrieve(name: String): String? =
    list.firstNotNullOfOrNull { it.retrieve(name) }

  override fun list(name: String): List<String>? = list.firstNotNullOfOrNull { it.list(name) }
}
