package tech.codingzen.kata.konfig.retriever

import tech.codingzen.kata.konfig.*
import tech.codingzen.kata.konfig.StringParsers.parser

inline fun <reified V : Any?> KataKonf.getParser(key: String, crossinline parser: ContextualParser<String, V>) =
  genericContextualParser(KataKonfRetrieversLens, key, parser)

inline operator fun <reified V : Any?> KataKonf.get(key: String, crossinline parser: ValueParser<String, V>) =
  genericValueParser(KataKonfRetrieversLens, key, parser)

inline operator fun <reified V : Any?> KataKonf.get(key: String) =
  get<V>(key, parser())

fun KataKonf.listOfStrings(key: String) = listOfStringsParser(KataKonfRetrieversLens, key)
