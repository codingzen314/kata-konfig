package tech.codingzen.kata.konfig.retriever

interface KataKonfRetriever {
  fun retrieve(name: String): String?

  fun list(name: String): List<String>?
}
