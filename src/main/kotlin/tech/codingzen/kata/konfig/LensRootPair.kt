package tech.codingzen.kata.konfig

import tech.codingzen.kata.konfig.annotations.InternalKataKonf

class LensRootPair @InternalKataKonf internal constructor(
  @InternalKataKonf
  internal val id: KataKonfRootId,
  @InternalKataKonf
  internal val root: KataKonfRoot
) {
  companion object {
    operator fun <ROOT> invoke(lens: KataKonfLens<ROOT>, root: ROOT) = LensRootPair(lens.id, root as KataKonfRoot)
  }
}