package tech.codingzen.kata.konfig.system_properties

import tech.codingzen.kata.konfig.KataKonfLens

object SystemPropertiesLens : KataKonfLens.Delegate<SystemPropertiesSource> {
  override val lens = KataKonfLens<SystemPropertiesSource>("SystemProperties")
}