package tech.codingzen.kata.konfig.system_properties

import tech.codingzen.kata.konfig.retriever.KataKonfRetriever

@JvmInline
value class SystemPropertiesSource(val source: Map<String, String>): KataKonfRetriever {
  companion object {
    @Suppress("UNCHECKED_CAST")
    val system: SystemPropertiesSource = SystemPropertiesSource(System.getProperties().toMap() as Map<String, String>)
  }

  operator fun get(key: String) = source[key]
  operator fun contains(key: String) = key in source
  override fun retrieve(name: String): String? = get(name)
  override fun list(name: String): List<String>? = null
}
