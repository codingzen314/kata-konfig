package tech.codingzen.kata.konfig.system_properties

import tech.codingzen.kata.konfig.*
import tech.codingzen.kata.konfig.StringParsers.parser

inline fun <reified V : Any?> KataKonf.syspropParser(key: String, crossinline parser: ContextualParser<String, V>) =
  genericContextualParser(SystemPropertiesLens, key, parser)

inline fun <reified V : Any?> KataKonf.sysprop(key: String, crossinline parser: ValueParser<String, V>) =
  genericValueParser(SystemPropertiesLens, key, parser)

inline fun <reified V : Any?> KataKonf.sysprop(key: String) =
  sysprop<V>(key, parser())