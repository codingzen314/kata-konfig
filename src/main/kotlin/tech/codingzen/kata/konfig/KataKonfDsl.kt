package tech.codingzen.kata.konfig

/**
 * Dsl that facilitates building a [KataKonf] instance
 */
class KataKonfDsl {
  private val sources = mutableMapOf<KataKonfRootId, KataKonfRoot>()

  companion object {
    operator fun invoke(block: KataKonfDsl.() -> Unit): KataKonf =
      KataKonfDsl().apply(block).build()
  }

  /**
   * @param ROOT type of root that the receiver will retrieve from the built [KataKonf] instance
   * @param root root that is associated with the receiver
   */
  fun <ROOT> KataKonfLens<ROOT>.source(root: ROOT) {
    sources[this.id] = root as KataKonfRoot
  }

  operator fun LensRootPair.unaryPlus() {
    sources[id] = root
  }

  /**
   * @return the built [KataKonf] instance
   */
  fun build() = KataKonf(sources)
}