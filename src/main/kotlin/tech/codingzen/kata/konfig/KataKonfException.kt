package tech.codingzen.kata.konfig

import tech.codingzen.kata.result.Err
import tech.codingzen.kata.result.parts

/**
 * Exception thrown when invoking a KataKonf PropertyDelegateProvider
 */
class KataKonfException : Exception {
  constructor() : super()
  constructor(message: String) : super(message)
  constructor(message: String, cause: Throwable) : super(message, cause)
  constructor(cause: Throwable) : super(cause)

  companion object {
    operator fun invoke(err: Err): KataKonfException {
      val (messages, cause) = err.parts
      val message = messages.joinToString(System.lineSeparator())
      return if (cause == null) KataKonfException(message) else KataKonfException(message, cause)
    }
  }
}