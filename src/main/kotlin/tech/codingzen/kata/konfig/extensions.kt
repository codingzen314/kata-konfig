package tech.codingzen.kata.konfig

import tech.codingzen.kata.konfig.annotations.EvaluateAlways
import tech.codingzen.kata.konfig.annotations.Prefix
import tech.codingzen.kata.konfig.retriever.KataKonfRetriever
import tech.codingzen.kata.result.*
import kotlin.properties.PropertyDelegateProvider
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.hasAnnotation

/**
 * @param V property type
 * @param providerFn fn used to provide the value that is returned by the built PropertyDelegateProvider
 * @return a PropertyDelegateProvider that executes providerFn and handles all errors by throwing a [KataKonfException].
 *
 * This PropertyDelegateProvider also handles properties annotated with [EvaluateAlways].  If the property is annotated
 * with [EvaluateAlways] then [providerFn] will always be invoked.  If the property is not annotated with
 * [EvaluateAlways] then [providerFn] will onlye be invoked once
 */
inline fun <reified V : Any?> propertyDelegateParser(crossinline providerFn: ResDsl.(Any, KProperty<*>) -> V) =
  PropertyDelegateProvider<Any, ReadOnlyProperty<Any, V>> { thisRef, property ->
    val logic = {
      when (val outcome = res { providerFn(this, thisRef, property) }) {
        is Ok -> outcome.value
        is Err -> {
          val className = thisRef?.let { it::class.qualifiedName } ?: "anonymous-type"
          throw KataKonfException(outcome.context("Attempting to set value for $className.${property.name}"))
        }
      }
    }

    if (property.hasAnnotation<EvaluateAlways>()) ReadOnlyProperty { _, _ -> logic() }
    else lazy(logic).let { lz -> ReadOnlyProperty { _, _ -> lz.value } }
  }

inline fun <reified V : Any?, ROOT : KataKonfRetriever, LENS : KataKonfLens<ROOT>> KataKonf.genericContextualParser(
  lens: LENS,
  key: String,
  crossinline parser: ContextualParser<String, V>
) = propertyDelegateParser { thisRef, property ->
  val builtKey = StringBuilder().apply {
    this@genericContextualParser.prefix?.run {
      append(this); append('.')
    }
    (property.findAnnotation<Prefix>()?.prefix ?: thisRef::class.findAnnotation<Prefix>()?.prefix)?.run {
      append(this); append('.')
    }
    append(key)
  }.toString()

  val root = lens[this@genericContextualParser].value()
  val src = root.retrieve(builtKey)
  parser(thisRef, property, src)
}

fun <ROOT : KataKonfRetriever, LENS : KataKonfLens<ROOT>> KataKonf.listOfStringsParser(
  lens: LENS,
  key: String
) = propertyDelegateParser { thisRef, property ->
  val builtKey = StringBuilder().apply {
    this@listOfStringsParser.prefix?.run {
      append(this); append('.')
    }
    (property.findAnnotation<Prefix>()?.prefix ?: thisRef::class.findAnnotation<Prefix>()?.prefix)?.run {
      append(this); append('.')
    }
    append(key)
  }.toString()

  val root = lens[this@listOfStringsParser].value()
  root.list(builtKey) ?: throw KataKonfException("No list value for Key: $key")
}

inline fun <reified V : Any?, ROOT : KataKonfRetriever, LENS : KataKonfLens<ROOT>> KataKonf.genericValueParser(
  lens: LENS,
  key: String,
  crossinline parser: ValueParser<String, V>
) =
  genericContextualParser(lens, key) { _, property, src ->
    src?.let { parser(it) } ?: run {
      if (property.returnType.isMarkedNullable) null as V
      else err("env: src for key: $key is null yet the return type is not marked as nullable!!")
    }
  }

inline fun <reified V : Any?> KataKonf.nested(path: String, crossinline parser: (KataKonf) -> V) =
  propertyDelegateParser { thisRef, property ->
    val builtPath = StringBuilder().apply {
      this@nested.prefix?.run {
        append(this); append('.')
      }
      (property.findAnnotation<Prefix>()?.prefix ?: thisRef::class.findAnnotation<Prefix>()?.prefix)?.run {
        append(this); append('.')
      }
      append(path)
    }.toString()

    parser(withPrefix(builtPath))
  }
