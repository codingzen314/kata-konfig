package tech.codingzen.kata.konfig.env

import tech.codingzen.kata.konfig.retriever.KataKonfRetriever

@JvmInline
value class EnvVarSource(val source: Map<String, String>): KataKonfRetriever {
  companion object {
    val system: EnvVarSource = EnvVarSource(System.getenv())
  }

  operator fun get(key: String) = source[key]
  operator fun contains(key: String) = key in source
  override fun retrieve(name: String): String? = get(name)
  override fun list(name: String): List<String>? = null
}
