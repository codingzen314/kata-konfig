package tech.codingzen.kata.konfig.env

import tech.codingzen.kata.konfig.KataKonfLens

object EnvVarLens : KataKonfLens.Delegate<EnvVarSource> {
  override val lens = KataKonfLens<EnvVarSource>("EnvVar")
}