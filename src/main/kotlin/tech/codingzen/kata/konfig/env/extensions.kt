package tech.codingzen.kata.konfig.env

import tech.codingzen.kata.konfig.*
import tech.codingzen.kata.konfig.StringParsers.parser

inline fun <reified V : Any?> KataKonf.envParser(key: String, crossinline parser: ContextualParser<String, V>) =
  genericContextualParser(EnvVarLens, key, parser)

inline fun <reified V : Any?> KataKonf.env(key: String, crossinline parser: ValueParser<String, V>) =
  genericValueParser(EnvVarLens, key, parser)

inline fun <reified V : Any?> KataKonf.env(key: String) =
  env<V>(key, parser())