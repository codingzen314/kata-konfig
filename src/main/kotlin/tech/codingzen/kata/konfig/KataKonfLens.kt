package tech.codingzen.kata.konfig

import tech.codingzen.kata.result.Res
import tech.codingzen.kata.result.res
import java.util.*
import kotlin.reflect.KClass

/**
 * Accessor for a ROOT instance associated inside a [KataKonf] instance
 */
interface KataKonfLens<out ROOT> {
  val id: String
  operator fun get(konf: KataKonf): Res<ROOT>

  companion object {
    inline operator fun <reified ROOT> invoke(alias: String): KataKonfLens<ROOT> = Impl(alias, ROOT::class)
  }

  interface Delegate<out ROOT> : KataKonfLens<ROOT> {
    val lens: KataKonfLens<ROOT>

    override val id: String
      get() = lens.id

    override fun get(konf: KataKonf): Res<ROOT> = lens[konf]
  }

  class Impl<out ROOT>(private val alias: String, private val rootClass: KClass<*>) : KataKonfLens<ROOT> {
    override val id: String = alias + UUID.randomUUID().toString()

    override fun get(konf: KataKonf): Res<ROOT> = res {
      val root = konf.getRoot(id) ?: err("KataKonf aliased by $alias needs a source added for ${rootClass.qualifiedName} into KataKonf")
      @Suppress("UNCHECKED_CAST")
      root as ROOT
    }
  }
}