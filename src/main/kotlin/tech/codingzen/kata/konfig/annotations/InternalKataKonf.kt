package tech.codingzen.kata.konfig.annotations

/**
 * Indicates that the annotated element is internal to KataKonf and as such should not be used by clients of this library.
 * Any and all elements annotated with [InternalKataKonf] are subject to change without regard for backwards compatibility
 */
@Target(AnnotationTarget.FIELD, AnnotationTarget.PROPERTY, AnnotationTarget.TYPE, AnnotationTarget.FUNCTION, AnnotationTarget.CLASS, AnnotationTarget.CONSTRUCTOR)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class InternalKataKonf