package tech.codingzen.kata.konfig.annotations

@Target(AnnotationTarget.CLASS,  AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.PROPERTY)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class Prefix(val prefix: String)
