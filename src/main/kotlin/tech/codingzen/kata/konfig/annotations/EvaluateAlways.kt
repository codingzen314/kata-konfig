package tech.codingzen.kata.konfig.annotations

/**
 * Indicates that the property delegate will always be executed when the property getter is invoked
 */
@Target(AnnotationTarget.FIELD, AnnotationTarget.PROPERTY)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class EvaluateAlways