package tech.codingzen.kata.konfig

import tech.codingzen.kata.konfig.annotations.InternalKataKonf
import kotlin.properties.ReadOnlyProperty

/**
 * An abstraction of the mapping of [KataKonfRoot] instances
 *
 * To create an instance of [KataKonf] please use [KataKonfDsl]
 */
class KataKonf(
  private val sources: MutableMap<KataKonfRootId, KataKonfRoot>,
  val prefix: String? = null
) {
  @InternalKataKonf
  fun getRoot(id: KataKonfRootId): KataKonfRoot? = sources[id]

  fun withPrefix(prefix: String) = KataKonf(sources, (this.prefix ?: "") + prefix)
}
