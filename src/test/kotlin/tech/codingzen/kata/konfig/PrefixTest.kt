package tech.codingzen.kata.konfig

import org.junit.Assert.assertEquals
import org.junit.Test
import tech.codingzen.kata.konfig.annotations.Prefix
import tech.codingzen.kata.konfig.env.*

class PrefixTest {
  @Test
  fun test() {
    val konf = KataKonfDsl {
      EnvVarLens.source(
        EnvVarSource(
          mapOf(
            "foo" to "12",
            "p0.f1" to "15",
            "nested.p1.f0" to "24",
            "nested.f1" to "25",
            "p2.f0" to "30",
            "baz.f1" to "31"
          )
        )
      )
    }
    TestA(konf).run {
      assertEquals(12, f0)
      assertEquals(15, f1)
      f2.run {
        assertEquals(24, f0)
        assertEquals(25, f1)
      }
    }
    TestC(konf).run {
      assertEquals(30, f0)
      assertEquals(31, f1)
    }
  }

  class TestA(konf: KataKonf) {
    val f0: Int by konf.env("foo")
    @Prefix("p0")
    val f1: Int by konf.env("f1")
    val f2: TestB by konf.nested("nested", ::TestB)
  }

  class TestB(konf: KataKonf) {
    @Prefix("p1")
    val f0: Int by konf.env("f0")
    val f1: Int by konf.env("f1")
  }

  @Prefix("baz")
  class TestC(konf: KataKonf) {
    @Prefix("p2")
    val f0: Int by konf.env("f0")
    val f1: Int by konf.env("f1")
  }
}