package tech.codingzen.kata.konfig

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test
import tech.codingzen.kata.konfig.args.ProgramArgsLens
import tech.codingzen.kata.konfig.args.arg
import java.math.BigDecimal

class ProgramArgsTest {
  @Test
  fun test() {
    val konf = KataKonfDsl {
      ProgramArgsLens.source(arrayOf("1", "abc", "3.14"))
    }
    TestA(konf).run {
      assertEquals(1, a)
      assertEquals("ABC", b)
      assertEquals(BigDecimal("3.14"), c)
      assertNull(d)
    }
  }

  class TestA(konf: KataKonf) {
    val a: Int by konf.arg(0)
    val b: String by konf.arg(1) { it.uppercase() }
    val c: BigDecimal by konf.arg(2)
    val d: Char? by konf.arg(3)
  }
}