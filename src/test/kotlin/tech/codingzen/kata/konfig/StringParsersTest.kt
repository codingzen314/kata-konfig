package tech.codingzen.kata.konfig

import org.junit.Test
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import tech.codingzen.assertIsErr
import tech.codingzen.assertIsOk
import tech.codingzen.assertOk
import tech.codingzen.kata.konfig.StringParsers.bigDecimal
import tech.codingzen.kata.konfig.StringParsers.bigInteger
import tech.codingzen.kata.konfig.StringParsers.boolean
import tech.codingzen.kata.konfig.StringParsers.byte
import tech.codingzen.kata.konfig.StringParsers.char
import tech.codingzen.kata.konfig.StringParsers.double
import tech.codingzen.kata.konfig.StringParsers.enum
import tech.codingzen.kata.konfig.StringParsers.float
import tech.codingzen.kata.konfig.StringParsers.int
import tech.codingzen.kata.konfig.StringParsers.long
import tech.codingzen.kata.konfig.StringParsers.parser
import tech.codingzen.kata.konfig.StringParsers.short
import tech.codingzen.kata.konfig.StringParsers.string
import tech.codingzen.kata.konfig.StringParsers.uByte
import tech.codingzen.kata.konfig.StringParsers.uInt
import tech.codingzen.kata.konfig.StringParsers.uLong
import tech.codingzen.kata.konfig.StringParsers.uShort
import tech.codingzen.kata.result.res
import java.math.BigDecimal
import java.math.BigInteger
import kotlin.reflect.KProperty
import kotlin.reflect.KType

class StringParsersTest {
  @Test
  fun stringOrNull() {
    assertOk("abc", string("abc"))
  }

  @Test
  fun byteOrNullTest() {
    assertOk(1.toByte(), byte("1"))
    assertIsErr(byte("a"))
  }

  @Test
  fun uByteOrNullTest() {
    assertOk(2.toUByte(), uByte("2"))
    assertIsErr(uByte("a"))
  }

  @Test
  fun shortOrNullTest() {
    assertOk(1.toShort(), short("1"))
    assertIsErr(short("a"))
  }

  @Test
  fun uShortOrNullTest() {
    assertOk(1.toUShort(), uShort("1"))
    assertIsErr(uShort("a"))
  }

  @Test
  fun intOrNullTest() {
    assertOk(1, int("1"))
    assertIsErr(int("a"))
  }

  @Test
  fun uIntOrNullTest() {
    assertOk(2.toUInt(), uInt("2"))
    assertIsErr(uInt("a"))
  }

  @Test
  fun longOrNullTest() {
    assertOk(1.toLong(), long("1"))
    assertIsErr(long("a"))
  }

  @Test
  fun uLongOrNullTest() {
    assertOk(2.toULong(), uLong("2"))
    assertIsErr(uLong("a"))
  }

  @Test
  fun floatOrNullTest() {
    assertIsOk(float("1.354"))
    assertIsErr(float("a"))
  }

  @Test
  fun doubleOrNullTest() {
    assertIsOk(double("2.345"))
    assertIsErr(double("a"))
  }

  @Test
  fun booleanOrNullTest() {
    assertOk(true, boolean("true"))
    assertOk(false, boolean("false"))
    assertOk(false, boolean("False"))
    assertOk(true, boolean("trUe"))
    assertIsErr(boolean("1"))
  }

  @Test
  fun charOrNullTest() {
    assertOk('c', char("cat"))
    assertIsErr(char(""))
  }

  @Test
  fun bigIntegerOrNullTest() {
    assertOk(1.toBigInteger(), bigInteger("1"))
    assertIsErr(bigInteger("a"))
  }

  @Test
  fun bigDecimalOrNullTest() {
    assertOk(BigDecimal("1.23"), bigDecimal("1.23"))
    assertIsErr(bigDecimal("a"))
  }

  @Test
  fun parserTest() {
    run {
      val nonNullType = mock<KType> {
        on { isMarkedNullable } doReturn false
      }
      val property = mock<KProperty<*>> {
        on { returnType } doReturn nonNullType
      }
      assertOk("abc", res { parser<String>()("abc") })
      assertOk(1.toByte(), res { parser<Byte>()("1") })
      assertIsErr(res { parser<Byte>()("a") })
      assertOk(2.toUByte(), res { parser<UByte>()("2") })
      assertIsErr(res { parser<UByte>()("a") })
      assertOk(1.toShort(), res { parser<Short>()("1") })
      assertIsErr(res { parser<Short>()("a") })
      assertOk(1.toUShort(), res { parser<UShort>()("1") })
      assertIsErr(res { parser<UShort>()("a") })
      assertOk(1, res { parser<Int>()("1") })
      assertIsErr(res { parser<Int>()("a") })
      assertOk(2.toUInt(), res { parser<UInt>()("2") })
      assertIsErr(res { parser<UInt>()("a") })
      assertOk(1.toLong(), res { parser<Long>()("1") })
      assertIsErr(res { parser<Long>()("a") })
      assertOk(2.toULong(), res { parser<ULong>()("2") })
      assertIsErr(res { parser<ULong>()("a") })
      assertIsOk(res { parser<Float>()("1.354") })
      assertIsErr(res { parser<Float>()("a") })
      assertIsOk(res { parser<Double>()("2.345") })
      assertIsErr(res { parser<Double>()("a") })
      assertOk(true, res { parser<Boolean>()("true") })
      assertOk(false, res { parser<Boolean>()("false") })
      assertOk(false, res { parser<Boolean>()("False") })
      assertOk(true, res { parser<Boolean>()("trUe") })
      assertOk('c', res { parser<Char>()("cat") })
      assertIsErr(res { parser<Char>()("") })
      assertOk(1.toBigInteger(), res { parser<BigInteger>()("1") })
      assertIsErr(res { parser<BigInteger>()("a") })
      assertOk(BigDecimal("1.23"), res { parser<BigDecimal>()("1.23") })
      assertIsErr(res { parser<BigDecimal>()("a") })
    }
  }

  @Test
  fun enumTest() {
    run {
      val nonNullType = mock<KType> {
        on { isMarkedNullable } doReturn true
      }
      val property = mock<KProperty<*>> {
        on { returnType } doReturn nonNullType
      }
      assertOk(TestEnumA0.Red, enum<TestEnumA0, TestEnumA0>()("Red"))
      assertIsErr(enum<TestEnumA0, TestEnumA0>()("1"))
    }

    run {
      val nonNullType = mock<KType> {
        on { isMarkedNullable } doReturn false
      }
      val property = mock<KProperty<*>> {
        on { returnType } doReturn nonNullType
      }
      assertOk(TestEnumA0.Red, enum<TestEnumA0, TestEnumA0>()("Red"))
      assertIsErr(enum<TestEnumA0, TestEnumA0>()("1"))
    }
  }
}

enum class TestEnumA0 {
  Red, Green, Blue
}