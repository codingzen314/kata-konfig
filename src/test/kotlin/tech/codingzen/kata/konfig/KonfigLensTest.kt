package tech.codingzen.kata.konfig

import org.junit.Test
import tech.codingzen.assertOk

class KonfigLensTest {
  @Test
  fun set_and_get() {
    val lens = KataKonfLens<Int>("alias")
    val konf = KataKonfDsl {
      lens.source(2)
    }
    assertOk(2, lens[konf])
  }

  @Test
  fun multiple() {
    val intLens = KataKonfLens<Int>("int")
    val stringLens = KataKonfLens<String>("string")
    val konf = KataKonfDsl {
      intLens.source(2)
      stringLens.source("abc")
    }
    assertOk(2, intLens[konf])
    assertOk("abc", stringLens[konf])
  }
}