package tech.codingzen.kata.konfig

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test
import tech.codingzen.kata.konfig.annotations.EvaluateAlways
import tech.codingzen.kata.konfig.system_properties.SystemPropertiesLens
import tech.codingzen.kata.konfig.system_properties.SystemPropertiesSource
import tech.codingzen.kata.konfig.system_properties.sysprop
import java.math.BigDecimal

class SystemPropertiesTest {
  @Test
  fun test() {
    val konf = KataKonfDsl {
      SystemPropertiesLens.source(SystemPropertiesSource(mapOf("foo" to "1", "bar.a" to "abc", "baz" to "3.14")))
    }
    TestA(konf).run {
      assertEquals(1, a)
      assertEquals("ABC", b)
      assertEquals(BigDecimal("3.14"), c)
      assertNull(d)
      assertEquals("ABC", b)
      assertEquals(1, e)
      assertEquals(2, e)
    }
  }

  class TestA(konf: KataKonf) {
    var counter: Int = 0
    val a: Int by konf.sysprop("foo")
    val b: String by konf.sysprop("bar.a") {
      counter++
      it.uppercase()
    }
    val c: BigDecimal by konf.sysprop("baz")
    val d: Char? by konf.sysprop("nope")

    @EvaluateAlways
    val e: Int by konf.sysprop("foo") { counter++ }
  }
}