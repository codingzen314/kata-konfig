package tech.codingzen.kata.konfig

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test
import tech.codingzen.kata.konfig.env.EnvVarSource
import tech.codingzen.kata.konfig.retriever.*
import tech.codingzen.kata.konfig.system_properties.SystemPropertiesSource
import java.math.BigDecimal

class KataKonfRetrieversTest {
  @Test
  fun test() {
    val konf = KataKonfDsl {
      KataKonfRetrieversLens.source(
        ListKataKonfRetriever(
          listOf(
            EnvVarSource(mapOf("foo" to "1", "bar" to "abc")),
            SystemPropertiesSource(mapOf("foo" to "nope", "baz" to "3.14")),
            MultiSource(mapOf("aList" to listOf("1","2","3")))
          )
        )
      )
    }
    TestA(konf).run {
      assertEquals(1, a)
      assertEquals("ABC", b)
      assertEquals(BigDecimal("3.14"), c)
      assertNull(d)
      assertEquals(listOf("1","2","3"), e)
    }
  }

  class TestA(konf: KataKonf) {
    val a: Int by konf.get("foo")
    val b: String by konf.get("bar") { it.uppercase() }
    val c: BigDecimal by konf.get("baz")
    val d: Char? by konf.get("nope")
    val e: List<String> by konf.listOfStrings("aList")
  }
}

@JvmInline
value class MultiSource(private val source: Map<String, List<String>>): KataKonfRetriever {
  override fun retrieve(name: String): String? = null

  override fun list(name: String): List<String>? = source[name]
}
