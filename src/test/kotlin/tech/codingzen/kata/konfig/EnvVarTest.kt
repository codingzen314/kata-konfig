package tech.codingzen.kata.konfig

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test
import tech.codingzen.kata.konfig.env.*
import java.math.BigDecimal

class EnvVarTest {
  @Test
  fun test() {
    val konf = KataKonfDsl {
      EnvVarLens.source(EnvVarSource(mapOf("foo" to "1", "bar.a" to "abc", "baz" to "3.14")))
    }
    TestA(konf).run {
      assertEquals(1, a)
      assertEquals("ABC", b)
      assertEquals(BigDecimal("3.14"), c)
      assertNull(d)
      assertNull(f)
    }
  }

  class TestA(konf: KataKonf) {
    val a: Int by konf.env("foo")
    val b: String by konf.env("bar.a") { it.uppercase() }
    val c: BigDecimal by konf.env("baz")
    val d: Char? by konf.env("nope")
    val f: Int? by konf.envParser("fjk") { _, _, s -> null}
  }
}